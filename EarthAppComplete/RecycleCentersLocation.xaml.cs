﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Services.Maps;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace EarthAppComplete
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RecycleCentersLocation : Page
    {
        Geolocator geolocator;
        List<Location> locationList;
        public RecycleCentersLocation()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            getLocations();
            getRecycleMarkers();
            

        }

        private async void getRecycleMarkers()
        {
            string url = URLs.BASE_URL + URLs.RECYCLE_CENTERS + "?" + JsonValues.token + "=" + Values.token;
            JObject postObject = new JObject();
            //postObject.Add(JsonValues.token, Values.token);
            //postObject.Add(JsonValues.content_type, "multipart / form - data");
            //Debug.WriteLine(postObject.ToString());
            Debug.WriteLine(url);
            HttpClient http = new System.Net.Http.HttpClient();
            HttpResponseMessage response = await http.GetAsync(url);
            Debug.WriteLine(response.ToString());
            //response.EnsureSuccessStatusCode();
            String webresponse = await response.Content.ReadAsStringAsync();
            Debug.WriteLine("Response: " + webresponse.ToString());

            JObject o = JObject.Parse(webresponse);
            Debug.WriteLine("Here");


            JArray recycle_centers = (JArray)o.GetValue(JsonValues.recycle_centers);
            Debug.WriteLine("Here 1");

            locationList = new List<Location>();
            foreach (var item in recycle_centers.Children())
            {
                var itemProperties = item.Children<JProperty>();
                Device d = new Device();
                Debug.WriteLine("Here 2");

                JObject location = JObject.Parse(itemProperties.FirstOrDefault(x => x.Name == JsonValues.location).Value.ToString());
                Debug.WriteLine("Here 3");

                Location l = new Location();

                l.latitude = Convert.ToDouble(location.GetValue(JsonValues.latitude).ToString());
                Debug.WriteLine("Here 4");

                l.longitude = Convert.ToDouble(location.GetValue(JsonValues.longitude).ToString());
                Debug.WriteLine("Here 5");

                l.name = itemProperties.FirstOrDefault(x => x.Name == JsonValues.recycle_center_name).Value.ToString();
                Debug.WriteLine("Here 6");

                //getName
                locationList.Add(l);
            }
            foreach (Location l in locationList)
            {
                MapIcon mapIcon1 = new MapIcon();
                // Locate your MapIcon  
                mapIcon1.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/pushpin.png"));
                // Show above the MapIcon  
                mapIcon1.Title = l.name;
                // Setting up MapIcon location  
                mapIcon1.Location = new Geopoint(new BasicGeoposition()
                {
                    //Latitude = geoposition.Coordinate.Latitude, [Don't use]  
                    //Longitude = geoposition.Coordinate.Longitude [Don't use]  
                    Latitude = l.latitude,
                    Longitude = l.longitude
                });
                // Positon of the MapIcon  
                mapIcon1.NormalizedAnchorPoint = new Point(0.5, 0.5);
                MyMap.MapElements.Add(mapIcon1);
            }
        }
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        public async void getLocations() { 
            // Map Token for testing purpose,   
            // otherwise you'll get an alart message in Map Control  
            //Map token : AsP5bvPpMNyJBtSE2VCldjRN7RaAV6_zqVoJDpaz1yk1S5yn9BZ2qELm5CV73_pY
            //MyMap.MapServiceToken = "AsP5bvPpMNyJBtSE2VCldjRN7RaAV6_zqVoJDpaz1yk1S5yn9BZ2qELm5CV73_pY";
            MyMap.MapServiceToken = Values.maps_token;
            geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            try
            {
                // Getting Current Location  
                Geoposition geoposition = await geolocator.GetGeopositionAsync(
                    maximumAge: TimeSpan.FromMinutes(5),
                    timeout: TimeSpan.FromSeconds(10));

                MapIcon mapIcon = new MapIcon();
                // Locate your MapIcon  
                mapIcon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/pushpin.png"));
                // Show above the MapIcon  
                mapIcon.Title = "Current Location";
                // Setting up MapIcon location
                Debug.WriteLine(geoposition.Coordinate.Point.Position.Latitude + "");  
                mapIcon.Location = new Geopoint(new BasicGeoposition()
                {
                    //Latitude = geoposition.Coordinate.Latitude, [Don't use]  
                    //Longitude = geoposition.Coordinate.Longitude [Don't use]  
                    Latitude = geoposition.Coordinate.Point.Position.Latitude,
                    Longitude = geoposition.Coordinate.Point.Position.Longitude
                });
                // Positon of the MapIcon  
                mapIcon.NormalizedAnchorPoint = new Point(0.5, 0.5);
                MyMap.MapElements.Add(mapIcon);
                

                MapIcon mapIcon1 = new MapIcon();
                 // Locate your MapIcon  
                 mapIcon1.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/pushpin.png"));
                 // Show above the MapIcon  
                 mapIcon1.Title = "Current Location";
                 // Setting up MapIcon location  
                 mapIcon1.Location = new Geopoint(new BasicGeoposition()
                 {
                     //Latitude = geoposition.Coordinate.Latitude, [Don't use]  
                     //Longitude = geoposition.Coordinate.Longitude [Don't use]  
                     Latitude = 19.206304,
                     Longitude = 72.874818 
                 });
                 // Positon of the MapIcon  
                 mapIcon1.NormalizedAnchorPoint = new Point(0.5, 0.5);
                 MyMap.MapElements.Add(mapIcon1);




                // Showing in the Map  
                await MyMap.TrySetViewAsync(mapIcon.Location, 18D, 0, 0, MapAnimationKind.Bow);







                // Disable the ProgreesBar  
                progressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                // Set the Zoom Level of the Slider Control  
                //mySlider.Value = MyMap.ZoomLevel;  
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox("Location is turned off");
            }
            //base.OnNavigatedTo(e);
        }
        

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>



        // Locate Me Bottom App Bar  
        private async void LocateMe_Click(object sender, RoutedEventArgs e)
        {
            progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
            geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            try
            {
                Geoposition geoposition = await geolocator.GetGeopositionAsync(
                    maximumAge: TimeSpan.FromMinutes(5),
                    timeout: TimeSpan.FromSeconds(10));
                await MyMap.TrySetViewAsync(geoposition.Coordinate.Point, 18D);
                //mySlider.Value = MyMap.ZoomLevel;
                progressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox("Location service is turned off!");
            }
        }

        // Custom Message Dialog Box  
        private async void MessageBox(string message)
        {
            var dialog = new MessageDialog(message.ToString());
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => await dialog.ShowAsync());
        }

        private async void MyMap_MapTapped(MapControl sender, MapInputEventArgs args)
        {
            Geopoint pointToReverseGeocode = new Geopoint(args.Location.Position);

            // Reverse geocode the specified geographic location.  
            MapLocationFinderResult result =
                await MapLocationFinder.FindLocationsAtAsync(pointToReverseGeocode);

            var resultText = new StringBuilder();

            if (result.Status == MapLocationFinderStatus.Success)
            {
                resultText.AppendLine(result.Locations[0].Address.District + ", " + result.Locations[0].Address.Town + ", " + result.Locations[0].Address.Country);
            }

            MessageBox(resultText.ToString());
        }
    }
}
