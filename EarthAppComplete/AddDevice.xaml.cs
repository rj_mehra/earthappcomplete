﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace EarthAppComplete
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddDevice : Page
    {
        public string url = URLs.BASE_URL + URLs.DEVICE_ADD;
        public AddDevice()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {


        }
        private async void Login()
        {
            String category = addDeviceList.SelectedValue.ToString();
            String devicename = deviceNameBox.Text.ToString();
            //validation
            double powerconsumption;
            bool isDouble = Double.TryParse(powerConsumptionBox.Text, out powerconsumption);
            if (addDeviceList.SelectedItem == null || devicename == "" || powerConsumptionBox.Text.ToString() == "")
            {
                MessageDialog dialog = new MessageDialog("Insert all necessary fields");
                await dialog.ShowAsync();

            }
            else if (!isDouble)
            {
                MessageDialog dialog = new MessageDialog("Power consumtion must be numeric");
                await dialog.ShowAsync();

            }
            else {
                JObject postObject = new JObject();
                if (energyRatingList.SelectedItem == null)
                {
                    postObject.Add(JsonValues.energy_rating, null);

                }
                else
                {
                    postObject.Add(JsonValues.energy_rating, energyRatingList.SelectedValue.ToString());

                }
                postObject.Add(JsonValues.device_name, devicename);
                postObject.Add(JsonValues.device_category_id, addDeviceList.Tag + "");
                postObject.Add(JsonValues.power_consumption, powerconsumption);
                postObject.Add(JsonValues.content_type, "multipart / form - data");
                Debug.WriteLine(postObject.ToString());
                HttpClient http = new System.Net.Http.HttpClient();
                HttpResponseMessage response = await http.PostAsync(url, new StringContent(postObject + "", Encoding.UTF8, "application/json"));
                try
                {
                    response.EnsureSuccessStatusCode();
                    String webresponse = await response.Content.ReadAsStringAsync();
                    //JObject o = JObject.Parse(webresponse);
                    //String token = o.GetValue(JsonValues.token).ToString();
                    //Values.token = token;
                    ////Debug.WriteLine("ValidLogin");
                    Debug.WriteLine(webresponse);
                    Device d = new Device();
                    d.name = devicename;
                    DeviceDetail detail = new DeviceDetail();
                    detail.powerConsumption = powerconsumption;
                    Category cat = new Category();
                    cat.name = category;
                    detail.deviceCategory = cat;
                    d.deviceDetails = detail;
                    var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                    Object value = localSettings.Values["user"];
                    User user = (User)value;
                    user.listDevice.Add(d);
                    localSettings.Values["user"] = user;
                    var frame = (Frame)Window.Current.Content;
                    frame.Navigate(typeof(DeviceDetails));
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Kindly retry");
                    //MessageBox.Show("Invalid login credentials");
                    MessageDialog dialog = new MessageDialog("Kindly Retry");
                    await dialog.ShowAsync();
                }
            }
        }
        private void AppBarButton_Click2(object sender, RoutedEventArgs e)
        {
            deviceNameBox.Text = "";
            powerConsumptionBox.Text = "";
        }
    }
}
