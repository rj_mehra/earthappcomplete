﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Location
{
    public int id { get; set; }
    public string street { get; set; }
    public string zip { get; set; }
    public string city { get; set; }
    public string state { get; set; }
    public string country { get; set; }
    public Double latitude { get; set; }
    public Double longitude { get; set; }
    public String name { get; set; }
}