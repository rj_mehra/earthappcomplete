﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Problem
{
    public int id { get; set; }
    public string name { get; set; }
    public Pollutant standardPollutant { get; set; }
    public Double ideal { get; set; }
    public List<Pollutant> pollutantList = new List<Pollutant>();
}