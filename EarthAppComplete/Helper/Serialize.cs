﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

[DataContract]
public class Serialize
{
    [DataMember]
    public int id { get; set; }
    [DataMember]

    public string name { get; set; }

    [DataMember]
    List<Serialize2> list = new List<Serialize2>();

    [DataMember]
    Serialize2 s2 = new Serialize2();

}
