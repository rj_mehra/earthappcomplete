﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Cause
{
    public int id { get; set; }
    public Double cause_level { get; set; }
    public Pollutant pollutant { get; set; }
    public Problem problem { get; set; }
}
