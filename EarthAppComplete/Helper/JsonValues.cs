﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class JsonValues
{
    //login and signup
    public static string name="name";
    public static string username="email";
    public static string password = "password";
    public static string content_type="Content-Type";
    public static string token = "token";
    //devices
    public static string device_array = "devices";
    public static string device_category = "category";
    public static string device_problem_array = "problems";
    public static string power_consumption = "power_consumption";
    public static string device_name = "name";
    public static string device_id = "id";
    public static string category_id = "id";
    public static string category_name = "name";
    public static string detail_id = "id";
    public static string device_category_details = "details";
    public static string device_usage_array = "device_usage";
    public static string hours_usage = "hour_usage";
    public static string date = "date";
    public static string problem_id = "id";
    public static string problem_ideal = "ideal";
    public static string problem_name = "name";
    public static string problem_emission_rate = "emission_rate";
    //public static string problem_pollutant = "pollutant";
    public static string problem_pollutant_array = "pollutant";
    public static string problem_statndardpollutant = "standard_pollutant";
    public static string pollutant_id = "id";
    public static string pollutant_name = "name";
    public static string standardpollutant_id = "id";
    public static string standardpollutant_name = "name";
    public static string pollutant_cause_level = "cause_level";


    public static string device_category_id = "device_category_id";
    public static string energy_rating = "energy_rating";
    public static string recycle_centers = "recycle_centers";
    public static string location = "location";
    public static string latitude = "latitude";
    public static string longitude = "longitude";
    public static string recycle_center_name = "name";
}

