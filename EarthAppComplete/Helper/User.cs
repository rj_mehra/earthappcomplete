﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
//using System.Runtime.Serialization.Formatters.Binary;
//using System.Runtime.Serialization.Formatters.Soap;
//using System.Runtime.Serialization ;

[DataContract]
public class User 
{
    [DataMember]
    public int id { get; set; }
    [DataMember]

    public string name { get; set; }
    [DataMember]

    public string email { get; set; }
    [DataMember]

    public string password { get; set; }
    [DataMember]

    public List<ItemRecycle> listItemRecycle = new List<ItemRecycle>();
    [DataMember]

    public List<Device> listDevice = new List<Device>();
    public List<Device> Devices
    {
        set { listDevice = value; }
        get { return listDevice; }
    }
    public List<ItemRecycle> ItemRecycles
    {
        set { listItemRecycle = value; }
        get { return listItemRecycle; }
    }
    
}
