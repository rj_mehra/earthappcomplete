﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class DeviceDetail
{
    public int id { get; set; }
    public Double powerConsumption { get; set; }
    public Category deviceCategory { get; set; }
    public Double energyRating { get; set; }
}
