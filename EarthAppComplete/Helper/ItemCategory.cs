﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ItemCategory
{
    public int id { get; set; }
    public string name { get; set; }
    public float degradable { get; set; }
}