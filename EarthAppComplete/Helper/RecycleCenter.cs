﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class RecycleCenter
{
    public int id { get; set; }
    public string name { get; set; }
    public Location location { get; set; }
}