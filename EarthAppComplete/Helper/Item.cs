﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Item
{
    public int id { get; set; }
    public string name { get; set; }
    public ItemCategory itemCategory { get; set; }
}