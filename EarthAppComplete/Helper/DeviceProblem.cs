﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class DeviceProblem
{
    public int id { get; set; }
    public Double emission_rate { get; set; }
    public Problem problem { get; set; }
    public Device device { get; set; }
}