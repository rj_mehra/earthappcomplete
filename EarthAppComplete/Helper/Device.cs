﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Device
{
    public int id { get; set; }
    public string name { get; set; }
    public  DeviceDetail deviceDetails { get; set; }
    public List<Problem> problemList = new List<Problem>();
    public List<DeviceUsage> listDeviceUsage = new List<DeviceUsage>();
    public List<DeviceUsage> DeviceUsages
    {
        set { listDeviceUsage = value; }
        get { return listDeviceUsage; }
    }

}