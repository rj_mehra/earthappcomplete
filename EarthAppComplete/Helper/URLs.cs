﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    class URLs
    {
    public static String BASE_URL = "https://earth-app-rarp.herokuapp.com";
    public static String LOGIN = "/api/signin";
    public static String SIGNUP = "/api/signup";
    public static String DEVICES_RETRIEVE = "/api/devices";
    public static String DEVICE_ADD = "/api/device";
    public static String RECYCLE_CENTERS = "/api/rec-cen";
    public static String TOKEN_VALUE = "";

}

