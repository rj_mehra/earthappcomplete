﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace EarthAppComplete
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DeviceDetails : Page
    {
        public string url = URLs.BASE_URL + URLs.DEVICES_RETRIEVE + "?" + JsonValues.token + "=" + Values.token;
        List<string> devices = new List<string>();
        public DeviceDetails()
        {
            this.InitializeComponent();
            //getDevices();
            TestAPIs();
            noDevicesText.Visibility = Visibility.Collapsed;
        }
        private async void TestAPIs()
        {
            HttpClient http = new System.Net.Http.HttpClient();
            HttpResponseMessage response = await http.GetAsync(URLs.BASE_URL + "/api/test-new" );
            Debug.WriteLine("URL"+url);
            String webresponse = await response.Content.ReadAsStringAsync();
            Debug.WriteLine(webresponse.ToString());

        }
        private async void getDevices()
        {
            JObject postObject = new JObject();
            postObject.Add(JsonValues.token, Values.token);
            postObject.Add(JsonValues.content_type, "multipart / form - data");
            Debug.WriteLine(postObject.ToString());
            Debug.WriteLine(url);
            HttpClient http = new System.Net.Http.HttpClient();
            HttpResponseMessage response = await http.GetAsync(url);
            Debug.WriteLine(response.ToString());
            //response.EnsureSuccessStatusCode();
            String webresponse = await response.Content.ReadAsStringAsync();
            Debug.WriteLine("Response: " + webresponse.ToString());
            JObject o = JObject.Parse(webresponse);
            JArray devices_array = (JArray)o.GetValue(JsonValues.device_array);
            if (devices_array.Count > 0)
            {
                noDevicesText.Visibility = Visibility.Collapsed;
            }
            List<Device> deviceList = new List<Device>();
            foreach (var item in devices_array.Children())
            {
                var itemProperties = item.Children<JProperty>();
                Device d = new Device();
                int id = Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == JsonValues.device_id).Value.ToString());
                string name = itemProperties.FirstOrDefault(x => x.Name == JsonValues.device_name).Value.ToString();
                Double power_consumption = Convert.ToDouble(itemProperties.FirstOrDefault(x => x.Name == JsonValues.power_consumption).Value.ToString());
                d.id = id;
                d.name = name;
                //add to the list
                devices.Add(name);
                Category cat = new Category();
                JObject category = JObject.Parse(itemProperties.FirstOrDefault(x => x.Name == JsonValues.device_category).Value.ToString());
                cat.name = (String)category.GetValue(JsonValues.category_name);
                cat.id = Convert.ToInt32((String)category.GetValue(JsonValues.category_id));
                cat.details = (String)category.GetValue(JsonValues.device_category_details);
                DeviceDetail deviceDetail = new DeviceDetail();
                deviceDetail.id = Convert.ToInt32(itemProperties.FirstOrDefault(x => x.Name == JsonValues.detail_id).Value.ToString());
                deviceDetail.powerConsumption = power_consumption;
                deviceDetail.deviceCategory = cat;
                d.deviceDetails = deviceDetail;
                Debug.WriteLine("Before Items");
                JArray problem_array = JArray.Parse(itemProperties.FirstOrDefault(x => x.Name == JsonValues.device_problem_array).Value.ToString());
                Debug.WriteLine("Here 1");
                List<Problem> problemList = new List<Problem>();
                foreach (var item1 in problem_array.Children())
                {
                    Problem p = new Problem();
                    var itemProperties1 = item1.Children<JProperty>();
                    Debug.WriteLine("Inside problems 1");
                    id = Convert.ToInt32(itemProperties1.FirstOrDefault(x => x.Name == JsonValues.problem_id).Value.ToString());
                    Debug.WriteLine("Inside problems 2");

                    name = itemProperties1.FirstOrDefault(x => x.Name == JsonValues.problem_name).Value.ToString();
                    Debug.WriteLine("Inside problems 2");

                    Double ideal = Convert.ToDouble(itemProperties1.FirstOrDefault(x => x.Name == JsonValues.problem_ideal).Value.ToString());
                    Debug.WriteLine("Inside problems 3");

                    Double emission = Convert.ToDouble(itemProperties1.FirstOrDefault(x => x.Name == JsonValues.problem_emission_rate).Value.ToString());
                    Debug.WriteLine("Inside problems 4");

                    p.ideal = ideal;
                    p.id = id;
                    p.name = name;
                    Debug.WriteLine("Inside problems 5");

                    JObject standardpollutant = JObject.Parse(itemProperties1.FirstOrDefault(x => x.Name == JsonValues.problem_statndardpollutant).Value.ToString());
                    Debug.WriteLine("Inside problems 6");
           
                    Pollutant po = new Pollutant();
                    
                    po.id = Convert.ToInt32(standardpollutant.GetValue(JsonValues.standardpollutant_id).ToString());
                    
                    po.name = standardpollutant.GetValue(JsonValues.standardpollutant_name).ToString();
                    Debug.WriteLine("Inside problems 9");

                    p.standardPollutant = po;
                    JArray pollutant_array = JArray.Parse(itemProperties1.FirstOrDefault(x => x.Name == JsonValues.problem_pollutant_array).Value.ToString());
                    List<Pollutant> pollutantList = new List<Pollutant>();
                    foreach (var item2 in pollutant_array.Children())
                    {
                        var itemProperties2 = item2.Children<JProperty>();

                        Pollutant pollutant = new Pollutant();

                        pollutant.name = itemProperties2.FirstOrDefault(x => x.Name == JsonValues.pollutant_name).Value.ToString();

                        pollutant.cause_level = Convert.ToDouble(itemProperties2.FirstOrDefault(x => x.Name == JsonValues.pollutant_cause_level).Value.ToString());

                        pollutantList.Add(pollutant);   
                    }

                    p.pollutantList = pollutantList;
                    problemList.Add(p);

                }
                d.problemList = problemList;
                Debug.WriteLine("Before DeviceUsage");
                JArray usage_array = JArray.Parse(itemProperties.FirstOrDefault(x => x.Name == JsonValues.device_usage_array).Value.ToString());
                Debug.WriteLine("Here 2");

                List<DeviceUsage> deviceUsageList = new List<DeviceUsage>();
                foreach (var item1 in usage_array.Children())
                {
                    var itemProperties1 = item1.Children<JProperty>();
                    DeviceUsage usage = new DeviceUsage();
                    usage.date = itemProperties1.FirstOrDefault(x => x.Name == JsonValues.date).Value.ToString();
                    usage.hourUsage = itemProperties1.FirstOrDefault(x => x.Name == JsonValues.hours_usage).Value.ToString();
                    deviceUsageList.Add(usage);
                }
                d.listDeviceUsage = deviceUsageList;
                deviceList.Add(d);
            }
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            //Object value = localSettings.Values["user"];
            //User user = (User)value;
            //user.Devices = deviceList;
            //localSettings.Values["user"] = user;

            listBox.ItemsSource = devices;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        private void addClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(deviceDetailsView), null);
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RecycleCentersLocation));
        }

        private void AppBarButton_Click_2(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(addDeviceUsage), null);
        }
    }
}
