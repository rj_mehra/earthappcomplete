﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace EarthAppComplete
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class menuPage : Page
    {
        public menuPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void devicesMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(DeviceDetails), null);
        }

        private void statusMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(DeviceDetails), null);
        }

        private void plantMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(plantTree), null);
        }

        private void recycleMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RecycleCentersLocation), null);
        }

        private void impactMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(userImpact), null);
        }

        private void SettingsMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(settings), null);
        }

        private void AboutMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(about), null);
        }
    }
}
