﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace EarthAppComplete
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //String url = "http://www.theparkingproject.com/servertest";
        String url = URLs.BASE_URL + URLs.LOGIN;
        String username;
        public MainPage()
        {
            this.InitializeComponent();
            //initiate();
            //this.NavigationCacheMode = NavigationCacheMode.Required;
        }
        private void initiate()
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            Object value = localSettings.Values["token"];

            if (value == null)
            {
                Debug.WriteLine("!Already exist");
                //return false;
            }
            else
            {
                // Access data in value
                Debug.WriteLine(value.ToString());
                Values.token = value.ToString();
                try
                {
                    Debug.WriteLine("Already exist");
                    //var frame = (Frame)Window.Current.Content;
                    Frame.Navigate(typeof(menuPage));
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                }

            }
        }
        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
         
            //this.Frame.Navigate(typeof(landingPage), null);
            //ProgressBar bar = new ProgressBar();
            /*ProgressIndicator = new ProgressIndicator();
            SystemTray.ProgressIndicator.Text = "Acquiring";
            */
            Login();
            //TestAPIs();
            //this.Frame.Navigate(typeof(deviceDetails), null);
        }
        

        private async void Login()
        {
            username = userNameText.Text.ToString();
            String password = passWordText.Password.ToString();
            //validation
            if (username == "" || password == "")
            {
                MessageDialog dialog = new MessageDialog("Insert both email and password");
                await dialog.ShowAsync();
                //MessageBox.Show("Insert both email and password");
            }
            else if (!Regex.IsMatch(username, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                //MessageBox.Show("Invalid Email");
                MessageDialog dialog = new MessageDialog("Invalid Email");
                await dialog.ShowAsync();
            }
            else {
                JObject postObject = new JObject();
                postObject.Add(JsonValues.username, username);
                postObject.Add(JsonValues.password, password);
                postObject.Add(JsonValues.content_type, "multipart / form - data");
                Debug.WriteLine(postObject.ToString());
                HttpClient http = new System.Net.Http.HttpClient();
                Uri uri = new Uri(url);
                HttpResponseMessage response = await http.PostAsync(url, new StringContent(postObject + "", Encoding.UTF8, "application/json"));
                try
                {
                    response.EnsureSuccessStatusCode();
                    String webresponse = await response.Content.ReadAsStringAsync();
                    JObject o = JObject.Parse(webresponse);
                    String token = o.GetValue(JsonValues.token).ToString();
                    Values.token = token;
                    Debug.WriteLine("ValidLogin");
                    Debug.WriteLine(webresponse);
                    storeValues(token);
                    var frame = (Frame)Window.Current.Content;
                    frame.Navigate(typeof(menuPage));
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Invalid login");
                    //MessageBox.Show("Invalid login credentials");
                    MessageDialog dialog = new MessageDialog("Invalid Login credentials");
                    await dialog.ShowAsync();
                }
            }
        }
        private void storeValues(String token)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            // Create a simple setting
            localSettings.Values["token"] = token;
            //User user = new User();
            //user.email = username;
            //localSettings.Values["user"] = user;
            //
            Debug.WriteLine(localSettings.Values["token"]);
        }
        private void logOut()
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            localSettings.Values.Remove("token");
        }
        private void signupButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Signup), null);
        }


        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }
    }
}
