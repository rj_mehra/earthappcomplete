﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace EarthAppComplete
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Signup : Page
    {
        public String url = URLs.BASE_URL + URLs.SIGNUP;

        public Signup()
        {
            this.InitializeComponent();

        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void signupButton_Click(object sender, RoutedEventArgs e)
        {
            signUp();

        }

        private async void signUp()
        {
            String name = nameText.Text.ToString();
            String username = userNameText.Text.ToString();
            String password = passWordText.Password.ToString();

            if (username == "" || password == "" || name == "")
            {
                MessageDialog dialog = new MessageDialog("Insert both email and password");
                await dialog.ShowAsync();
                //MessageBox.Show("Insert both email and password");
            }
            else if (!Regex.IsMatch(username, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                //MessageBox.Show("Invalid Email");
                MessageDialog dialog = new MessageDialog("Invalid Email");
                await dialog.ShowAsync();
            }
            else if (password.Length < 6)
            {
                MessageDialog dialog = new MessageDialog("Password must be atleast 6 characters long");
                await dialog.ShowAsync();
            }
            else {
                JObject postObject = new JObject();
                postObject.Add(JsonValues.username, username);
                postObject.Add(JsonValues.password, password);
                postObject.Add(JsonValues.name, name);
                postObject.Add(JsonValues.content_type, "multipart / form - data");
                Debug.WriteLine(postObject.ToString());
                HttpClient http = new System.Net.Http.HttpClient();
                HttpResponseMessage response = await http.PostAsync(url, new StringContent(postObject + "", Encoding.UTF8, "application/json"));
                try
                {
                    response.EnsureSuccessStatusCode();
                    String webresponse = await response.Content.ReadAsStringAsync();
                    //JObject o = JObject.Parse(webresponse);
                    Debug.WriteLine("ValidSignup");
                    Debug.WriteLine(webresponse);
                    this.Frame.Navigate(typeof(MainPage), null);
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Email already used");
                    //MessageBox.Show("Invalid login credentials");
                    MessageDialog dialog = new MessageDialog("Email already used");
                    await dialog.ShowAsync();
                }
            }
        }
    }
}
